<?php
require "vendor/autoload.php";
require "bootstrap.php";
use Exam\Models\Product;


$app = new \Slim\App();
/*
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});*/

//שליפת נתונים
$app->get('/products', function($request, $response,$args){
    $_product = new Product();
    $product = $_product->all();
    $payload = [];
    foreach($product as $usr){
         $payload[$usr->id] = [
             'id'=>$usr->id,
             'name'=>$usr->name,
             'price'=>$usr->price,
             'created_at'=>$usr->created_at,
             'updated_at'=>$usr->updated_at
         ];
    }
    return $response->withStatus(200)->withJson($payload);
 });
 $app->put('/products/{id}', function($request, $response, $args){
    $product = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');
    $_product = Product::find($args['id']);
    $_product->name = $product;
    $_product->price = $price;
   
    if($_product->save()){
        $payload = ['id' => $_product->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});
 
$app->get('/products/{id}', function($request, $response,$args){    
    $_product = Product::find($args['id']);

    $payload=[];

    if($_product->id){
      return $response->withStatus(200)->withJson(json_decode($_product))->withHeader('Access-Control-Allow-Origin', '*');
    }else{
      return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }  
    
});

//הרשאה לאבטחת מידע same origin policy

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();
